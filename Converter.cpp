/*
 * ConverterStateMachine.cpp
 *
 *  Created on: Oct 14, 2022
 *      Author: npinillo
 */

//THIS IS THE IMPLEMENTATION FILE


//! @file   FgcEtherResponse.cpp
//! @brief  This is a helper class to decouple preparing and sending of FGC_Ether response packets.
//! @author Dariusz Zielinski

#include <iostream>
#include "Converter.h"
#include "fsm.h"
#include "myclasses.h"


using namespace std;

using namespace lpc;  //No puedo cambiar el namespace?
// **********************************************************

//! This function will update state machine. First, it will execute state function for the
//! current state. Then it'll iterate over transition functions for the current state, until some transition
//! changes the state. If that happens, the returned state becomes the new state.
//! If no cascade was requested, the function finishes. Otherwise, a state function of the new state is
//! executed and its transitions are checked.
//! Note: this has potential to become an infinite loop, if the FSM design is flawed.

    Converter::Converter() //Constructor: The role of a contructor is to ensure that the object is correctly initialized. (Todavia no he creado el objeto)
        : my_fsm(*this, State::turn_on)
    {
        //m_response.header.fgc.payload_type = FGC_ETHER_PAYLOAD_RSP;
        key='o';

        my_fsm.addState(State::turn_on, &Converter::turnOnState,        { &Converter::xxToFault, &Converter::turnOnToFullPower  });
        my_fsm.addState(State::full_power, &Converter::fullPowerState, { &Converter::xxToFault, &Converter::fullPowerToTurnOff });
        my_fsm.addState(State::turn_off,  &Converter::TurnOffState,  { &Converter::xxToFault, &Converter::turnOffToTurnOn });
        my_fsm.addState(State::fault,  &Converter::FaultState,  { &Converter::faultToOff});
    }



void Converter::CheckKeyboard()
{
    my_fsm.update();

    //return (m_state == Converter::responding || m_state == Converter::responding_done);
}


//char name=master;
// **********************************************************

//void CheckKeyboard(){
//	std>>cin>>name;
//}
// **********************************************************

/*fgc_ether_rsp* FgcEtherResponse::getResponseFrame()
{
    m_header.incrementSequence();
    m_header.transferToFrame(m_response, m_sending_length);
    return &m_response;
}*/

// **********************************************************

/*void FgcEtherResponse::copyDataIntoFrame()
{
    auto&   result          = m_executor.getResult();
    size_t additional_bytes = 0;

    // For the first packet, add timestamp at the beginning of the data in the response frame
    if (m_fsm.getState() == State::first_packet)
    {
        additional_bytes = sizeof(FgcEtherTimeStamp);

        auto time_stamp_ptr = reinterpret_cast<uint8_t*>(&result.time_stamp);
        std::copy(time_stamp_ptr, time_stamp_ptr + additional_bytes, m_response.payload.data.rsp_pkt);
    }

    // Calculate how many bytes to copy
    auto bytes_to_copy = result.response.end() -  m_sending_offset;

    if (bytes_to_copy > static_cast<int>(max_length - additional_bytes))
    {
        bytes_to_copy = max_length - additional_bytes;
    }

    // Copy data into frame
    std::copy(m_sending_offset, m_sending_offset + bytes_to_copy, m_response.payload.data.rsp_pkt + additional_bytes);

    // Advance the iterator
    m_sending_offset += bytes_to_copy;

    // Check if there is still more to be sent
    m_sending_left = (m_sending_offset != result.response.end());

    // Set how many bytes will finally be sent
    m_sending_length = bytes_to_copy + additional_bytes;
}*/

/*In the state functions you can do whatever is needed to do for the given state. They are executed when the state machine
enters given state. After the state function, all transition functions are executed until a function returns an object
that says that a transition should occur.*/

// **********************************************************
// WHAT TO DO IN EACH STAGE

void Converter::turnOnState()
{
	std::cout<< "Hello, I am ON" <<endl;
}

// **********************************************************

void Converter::fullPowerState()
{
	std::cout<< "Hello, I am FULL POWER" <<endl;
}

// **********************************************************

void Converter::TurnOffState()
{
	std::cout<< "Hello, I am OFF" <<endl;
}

// **********************************************************

void Converter::FaultState()
{
	std::cout<< "Warning, a FAULT has occurred" <<endl;
}
// **********************************************************
// DEFINITION OF TRANSITION FUNCTION

Converter::TransRes Converter::turnOnToFullPower()
{
    // Go to the first packet if the last status was sent with the FGC_OK_RSP
    // and the executor has finished executing the command.
    if(Converter::key=='a'){
        return State::full_power;
    }
    return{};
}
Converter::TransRes Converter::fullPowerToTurnOff()
{
    // Go to the first packet if the last status was sent with the FGC_OK_RSP
    // and the executor has finished executing the command.
    if(Converter::key=='u'){
         return State::turn_off;
    }
    return{};
}

// **********************************************************

Converter::TransRes Converter::xxToFault()
{
    if(Converter::key=='f'){
        return State::fault;
    }
    return{};
}

// **********************************************************

Converter::TransRes Converter::faultToOff()
{
    if(Converter::key=='u'){
        return State::turn_off;
    }
    return{};
}

// **********************************************************

Converter::TransRes Converter::turnOffToTurnOn()
{
    if(Converter::key=='p'){
        return lpc::Converter::State::turn_on;
    }
    return{};
}


// EOF
