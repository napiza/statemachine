#pragma once

//! @file   FgcEtherResponse.h
//! @brief  This is a helper class to decouple preparing and sending of FGC_Ether response packets.
//! @author Dariusz Zielinski

//#include <atomic>
#include <string>
#include "fsm.h"
/*#include <fgc_ether.h>
#include <FgcEtherState.h>
#include <FgcEtherStatus.h>
#include <FgcEtherHeader.h>
#include <FgcEtherExecutor.h>*/



    //! This class is used to create Finite State Machine and it's meant to be used from within another class
    //! which type is denoted by \p Parent template parameter.
    //! The FSM contains states and transitions. The update() method will iterate over each transition for the current
    //! state and execute them to check if the state should be changed. For each state a state function is executed.
    //! When making a transition, the transition can cascade - if so, then another iteration of update() function
    //! is executed, resulting in immediate execution of a state function of the new state and checking of transitions.
    //!


namespace lpc //namespace
{
    //! Preparation and state machine for the Converter control
    //!
    //! Finite State Machine graph:
    //!
    //!                               |>>>>>>>>>>>>>>>>>>>>>>> xxToLastPacket >>>>>>>>>>>>>>>>>>>>>>>|
    //!                               |                                                              |
    //!     |> idleToFirstPacket >|   |   |> firstToNextPacket >|     |>>>> xxToLastPacket >>>>|     |
    //!     |                     |   |   |                     |     |                        |     |
    //!  ********              ****************             ***************                ***************
    //!  * IDLE *              * FIRST PACKET *             * NEXT PACKET *                * LAST PACKET *
    //!  ********              ****************             ***************                ***************
    //!     |                                                                                     |
    //!     |<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< lastPacketToIdle <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<|
    //!

	//!
	//! Finite State Machine graph:
	//!  |>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> xxToFault >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>|
	//!  |                            |>>>>>>>>>>>>>>>>>>>>>>>>> xxToFault >>>>>>>>>>>>>>>>>>>>>>>>>>>|
	//!  |                            |                                                               |
	//!  |  |> turnOnToFullPower >|   |   |> fullPowerToTurnOff |     |>>>>>> xxToFault >>>>>>>|      |
	//!  |  |                     |   |   |                     |     |                        |      |
	//!  ********              ****************             ***************                ***************
	//!  * TURN ON *              * FULL POWER *             * TURN OFF *                * FAULT *
	//!  ********              ****************             ***************                ***************
	//!     |                                                   |<<<<<<<<<<<<< faultToOff <<<<<<<<<<<<|
    //!		|      												|
    //!     |                                                   |
	//!     |<<<<<<<<<<<<<<<<<< turnOffToTurnOn <<<<<<<<<<<<<<<<|
	//!
    class Converter
    {
       // static constexpr auto max_length = sizeof(fgc_fieldbus_rsp::rsp_pkt);
       // static constexpr bool cascade    = utils::FsmCascade;

        enum class State
        {
            turn_on,
            full_power,
            turn_off,
            fault
        };

        using TransRes = utils::FsmTransitionResult<State>;   // This class is used as a return type from transition function. It carries the information wheter the transition should take place

        public:
        	Converter(); //Declaration of the contructor inside the class

            void CheckKeyboard();

           // fgc_ether_rsp* getResponseFrame();
            char key;

        

        private:
            void turnOnState();    //ESTADOS

            void fullPowerState();

            void TurnOffState();

            void FaultState();

            TransRes turnOnToFullPower();  //TRANSICIONES

            TransRes fullPowerToTurnOff();

            TransRes xxToFault();

            TransRes faultToOff();

            TransRes turnOffToTurnOn();

            //FgcEtherStateAtomic&                m_state;
            //FgcEtherStatus&                     m_status;
            //FgcEtherExecutor&                   m_executor;

            // The finite state machine using FSM utility:
            utils::Fsm<State, Converter> my_fsm;
            /*fgc_ether_rsp                       m_response;
            FgcEtherRspHeader                   m_header;
            std::string::iterator               m_sending_offset;
            size_t                              m_sending_length = 0;
            bool                                m_sending_left = true;*/

       

    };

    

}


// EOF
